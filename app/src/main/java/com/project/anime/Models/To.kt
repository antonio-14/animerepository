package com.project.anime.Models

data class To(
    var day: Int? = null,
    var month: Int? = null,
    var year: Int? = null
)