package com.project.anime.Models

data class Jpg(
    var image_url: String? = null,
    var large_image_url: String? = null,
    var small_image_url: String? = null
)