package com.project.anime.Models

data class Aired(
    var from: String? = null,
    var prop: Prop? = null,
    var to: String? = null
)