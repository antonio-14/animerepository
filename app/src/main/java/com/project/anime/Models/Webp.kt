package com.project.anime.Models

data class Webp(
    var image_url: String? = null,
    var large_image_url: String? = null,
    var small_image_url: String? = null
)