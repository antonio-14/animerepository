package com.project.anime.Models

data class Trailer(
    var embed_url: String? = null,
    var url: String? = null,
    var youtube_id: String? = null
)