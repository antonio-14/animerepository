package com.project.anime.Models

data class Images(
    var jpg: Jpg? = null,
    var webp: Webp? = null
)