package com.project.anime.Models

data class From(
    var day: Int? = null,
    var month: Int? = null,
    var year: Int? = null
)