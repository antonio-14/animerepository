package com.project.anime.Models.Room

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class TopAnimeEntity(
    @PrimaryKey
    var id: Int = 0,
    var image: String = "",
    var name: String = "",
    var popularity: Int = 0,
    var isAired: Boolean = false,
    var rating: String = "",
    var synopsis: String = "",
    var type: String = "",
    var source: String = "",
    var rank: Int = 0,
    var trailer: String = ""
)