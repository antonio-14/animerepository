package com.project.anime.Models

data class Broadcast(
    var day: String? = null,
    var string: String? = null,
    var time: String? = null,
    var timezone: String? = null
)