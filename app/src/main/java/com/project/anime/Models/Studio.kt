package com.project.anime.Models

data class Studio(
    var mal_id: Int? = null,
    var name: String? = null,
    var type: String? = null,
    var url: String? = null
)