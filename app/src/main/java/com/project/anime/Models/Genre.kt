package com.project.anime.Models

data class Genre(
    var mal_id: Int? = null,
    var name: String? = null,
    var type: String? = null,
    var url: String? = null
)