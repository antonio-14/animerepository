package com.project.anime.Models

data class Prop(
    var from: From? = null,
    var string: String? = null,
    var to: To? = null
)