package com.project.anime.Models

data class Pagination(
    var has_next_page: Boolean? = null,
    var last_visible_page: Int? = null
)