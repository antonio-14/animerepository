package com.project.anime.Models

data class TopAnimeResponse(
    var data: List<Data>? = null,
    var pagination: Pagination? = null
)