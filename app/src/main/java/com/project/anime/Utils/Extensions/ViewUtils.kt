package com.project.anime.Utils.Extensions

import android.content.Context
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.project.anime.R

object ViewUtils {

    fun View.show() {
        visibility = View.VISIBLE
    }

    fun View.hide() {
        visibility = View.GONE
    }

    fun ImageView.setImagePlaceholderImg(image:String, context: Context) {
        Glide.with(context).load(image).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(this)
    }

}