package com.project.anime.Utils

import com.project.anime.Models.Data
import com.project.anime.Models.Room.TopAnimeEntity

object ModelUtils {

    fun getTopAnimeEntity(data: Data): TopAnimeEntity {
        return TopAnimeEntity(data.mal_id?:0,
        data.images?.jpg?.large_image_url?:"",
            data.title?:"", data.popularity?:0,
        data.airing?:false, data.rating?:"", data.synopsis?:"",
        data.type?:"", data.source?:"", data.rank?:0, data.trailer?.url?:"")
    }

}