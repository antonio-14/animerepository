package com.project.anime.Utils.domain

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.project.anime.Models.Room.TopAnimeEntity
import kotlinx.coroutines.flow.Flow


@Dao
interface AnimeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllAnime(anime: List<TopAnimeEntity>)

    @Query("SELECT * FROM TopAnimeEntity")
    fun getAllAnime(): MutableList<TopAnimeEntity>

    @Query("SELECT * FROM TopAnimeEntity WHERE id LIKE :animeId LIMIT 1")
    fun getAnimeById(animeId: Int): TopAnimeEntity

}