package com.project.anime.Utils.domain

import com.project.anime.Models.TopAnimeResponse
import retrofit2.http.GET

interface WebService {

    @GET("top/anime?page=1")
    suspend fun getTopAnime(): TopAnimeResponse

}