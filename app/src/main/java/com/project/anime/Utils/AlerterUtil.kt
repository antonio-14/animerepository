package com.project.anime.Utils

import android.app.Activity
import com.project.anime.R
import com.tapadoo.alerter.Alerter

class AlerterUtil {

    companion object {
        fun createAlertError(message: String, activity: Activity){
            Alerter.create(activity)
                .setTitle(R.string.error)
                .setText(message)
                .setDuration(4000)
                .setDismissable(true)
                .setBackgroundColorRes(R.color.red)
                .show()
        }
        fun createAlertWarning(message: String, activity: Activity){
            Alerter.create(activity)
                .setTitle(R.string.alert)
                .setText(message)
                .setDuration(4000)
                .setDismissable(true)
                .setBackgroundColorRes(R.color.alerter_default_success_background)
                .show()
        }
        fun createAlert(message: String, activity: Activity){
            Alerter.create(activity)
                .setDuration(4000)
                .setDismissable(true)
                .setText(message)
                .setBackgroundColorRes(R.color.alerter_default_success_background)
                .show()
        }
    }


}