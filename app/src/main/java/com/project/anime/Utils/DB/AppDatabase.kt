package com.project.anime.Utils.DB

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.project.anime.Models.Room.TopAnimeEntity
import com.project.anime.Utils.domain.AnimeDao

@Database(entities = [TopAnimeEntity::class], version = 4)
abstract class AppDatabase: RoomDatabase() {


    abstract fun animeDao(): AnimeDao

    companion object {

        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            INSTANCE = INSTANCE?: Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, "table_anime").fallbackToDestructiveMigration().build()
            return INSTANCE!!
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }

}