package com.project.anime.ModulDetailAnime

import com.project.anime.Models.Room.TopAnimeEntity
import com.project.anime.Utils.vo.Resource

class DetailAnimeInterfaces {

    interface DetailAnimeDataSourceI {
        suspend fun getAnime(animeId: Int): Resource.Success<TopAnimeEntity>
    }

    interface DetailAnimeRepoI {
        suspend fun getAnime(animeId: Int): Resource.Success<TopAnimeEntity>
    }

}