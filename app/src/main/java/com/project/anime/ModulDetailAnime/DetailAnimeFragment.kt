package com.project.anime.ModulDetailAnime

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.project.anime.Models.Room.TopAnimeEntity
import com.project.anime.ModulAllAnime.AllAnimeDataSource
import com.project.anime.ModulAllAnime.AllAnimeRepo
import com.project.anime.ModulAllAnime.AllAnimeViewModel
import com.project.anime.ModulAllAnime.AllAnimeViewModelFactory
import com.project.anime.R
import com.project.anime.Utils.DB.AppDatabase
import com.project.anime.Utils.Extensions.ViewUtils.hide
import com.project.anime.Utils.Extensions.ViewUtils.setImagePlaceholderImg
import com.project.anime.Utils.Extensions.ViewUtils.show
import com.project.anime.Utils.vo.Resource
import com.project.anime.databinding.FragmentAllAnimesBinding
import com.project.anime.databinding.FragmentDetailAnimeBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import android.content.Intent
import android.net.Uri
import androidx.navigation.fragment.findNavController
import com.project.anime.Utils.AlerterUtil
import java.lang.Exception


class DetailAnimeFragment : Fragment(R.layout.fragment_detail_anime) {

    private lateinit var binding: FragmentDetailAnimeBinding
    private var anime: TopAnimeEntity? = null

    private val viewModelDetailAnime by viewModels<DetailAnimeViewModel> { DetailAnimeViewModelFactory(
        DetailAnimeRepo(DetailAnimeDataSource(requireContext(),  AppDatabase.getDatabase(requireActivity().applicationContext)))
    ) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView(view)
        getAnime()
    }

    private fun setupView(view: View) {
        binding = FragmentDetailAnimeBinding.bind(view)
        binding.ivBack.setOnClickListener {
            findNavController().navigate(R.id.action_detailAnimeFragment_to_allAnimeFragment)
        }
    }

    private fun getAnime() {
        viewModelDetailAnime.getAnime(arguments?.getInt("animeId")?:0).observe(viewLifecycleOwner, { result ->
            when(result) {
                is Resource.Loading -> {
                    lifecycleScope.launch(Dispatchers.Main) {
                        binding.progressBarAnimeDetail.show()
                    }
                }
                is Resource.Success -> {
                    hideProgressBar()
                    anime = result.data
                    setupFields()
                }
                is Resource.Failure -> {
                    hideProgressBar()
                }
            }
        })
    }

    private fun setupFields() {
        binding.tvTitle.text = anime?.name?:""
        binding.tvRating.text = anime?.source?:""
        binding.tvType.text = anime?.type?:""
        binding.ivAnime.setImagePlaceholderImg(anime?.image?:"", requireContext())
        binding.ivAnime.clipToOutline = true
        binding.tvSipnosis.text = anime?.synopsis?:""
        binding.buttonWatchTrailer.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(anime?.trailer?:"")
            try {
                startActivity(intent)
            } catch (e: Exception) {
                AlerterUtil.createAlertError(getString(R.string.trailer_no_availlable), requireActivity())
            }
        }
    }

    private fun hideProgressBar() {
        lifecycleScope.launch(Dispatchers.Main) {
            binding.progressBarAnimeDetail.hide()
        }
    }
}