package com.project.anime.ModulDetailAnime

import android.content.Context
import com.project.anime.Models.Room.TopAnimeEntity
import com.project.anime.Utils.DB.AppDatabase
import com.project.anime.Utils.vo.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DetailAnimeDataSource(private val context: Context, private val appDatabase: AppDatabase): DetailAnimeInterfaces.DetailAnimeDataSourceI {
    override suspend fun getAnime(animeId: Int): Resource.Success<TopAnimeEntity> {
        var animeEntity: TopAnimeEntity
        withContext(Dispatchers.IO) {
            animeEntity = appDatabase.animeDao().getAnimeById(animeId)
        }
        return Resource.Success(animeEntity)
    }
}