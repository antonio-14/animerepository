package com.project.anime.ModulDetailAnime

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.project.anime.Utils.vo.Resource
import kotlinx.coroutines.Dispatchers

class DetailAnimeViewModel(private val repo: DetailAnimeRepo): ViewModel() {

    fun getAnime(animeId: Int) = liveData(viewModelScope.coroutineContext + Dispatchers.Main) {
        emit(Resource.Loading())
        try {
            emit(repo.getAnime(animeId))
        } catch (e: Exception) {
            emit(Resource.Failure(e))
        }
    }

}

class DetailAnimeViewModelFactory(private val repo: DetailAnimeRepo) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(DetailAnimeRepo::class.java).newInstance(repo)
    }
}