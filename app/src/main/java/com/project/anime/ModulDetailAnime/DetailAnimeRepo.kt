package com.project.anime.ModulDetailAnime

import com.project.anime.Models.Room.TopAnimeEntity
import com.project.anime.Utils.vo.Resource

class DetailAnimeRepo(private val dataSource: DetailAnimeDataSource): DetailAnimeInterfaces.DetailAnimeRepoI {
    override suspend fun getAnime(animeId: Int): Resource.Success<TopAnimeEntity> = dataSource.getAnime(animeId)
}