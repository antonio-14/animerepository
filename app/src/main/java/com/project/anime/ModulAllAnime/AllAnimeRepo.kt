package com.project.anime.ModulAllAnime

import com.project.anime.Models.Room.TopAnimeEntity
import com.project.anime.Utils.vo.Resource

class AllAnimeRepo(private val dataSource: AllAnimeDataSource): AllAnimeInterfaces.AllAnimeRepoI {
    override suspend fun getAllAnime(): Resource.Success<MutableList<TopAnimeEntity>> = dataSource.getAllAnime()
}