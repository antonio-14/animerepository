package com.project.anime.ModulAllAnime

import android.content.Context
import com.project.anime.Models.Room.TopAnimeEntity
import com.project.anime.Utils.DB.AppDatabase
import com.project.anime.Utils.ModelUtils
import com.project.anime.Utils.vo.Resource
import com.project.anime.Utils.vo.RetrofitClient
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AllAnimeDataSource(private val context: Context, private val appDatabase: AppDatabase): AllAnimeInterfaces.AllAnimeDataSourceI {
    override suspend fun getAllAnime(): Resource.Success<MutableList<TopAnimeEntity>> {
        var allAnimeEntity = mutableListOf<TopAnimeEntity>()
        withContext(Dispatchers.IO) {
            if (appDatabase.animeDao().getAllAnime().isEmpty()) {
                val topAnime = RetrofitClient.webService.getTopAnime()
                for (anime in topAnime.data?:mutableListOf()) {
                    val topAnime = ModelUtils.getTopAnimeEntity(anime)
                    allAnimeEntity.add(topAnime)
                }
                appDatabase.animeDao().insertAllAnime(allAnimeEntity.toList())
            } else {
                allAnimeEntity = appDatabase.animeDao().getAllAnime()
            }
        }
        return Resource.Success(allAnimeEntity)
    }
}