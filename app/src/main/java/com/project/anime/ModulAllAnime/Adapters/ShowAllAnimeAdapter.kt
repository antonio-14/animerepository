package com.project.anime.ModulAllAnime.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.project.anime.Models.Room.TopAnimeEntity
import com.project.anime.ModulAllAnime.AllAnimeInterfaces
import com.project.anime.R
import com.project.anime.Utils.Base.BaseViewHolder
import com.project.anime.Utils.Extensions.ViewUtils.setImagePlaceholderImg
import java.util.*

class ShowAllAnimeAdapter(
    private val context: Context,
    private val listener: AllAnimeInterfaces.ShowAllAnimeI
): RecyclerView.Adapter<BaseViewHolder<*>>() {

    var filteredItems: MutableList<TopAnimeEntity> = mutableListOf()
    var listAnime: MutableList<TopAnimeEntity> = mutableListOf()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return MainViewHolder(LayoutInflater.from(context).inflate(R.layout.item_anime, parent, false))
    }

    override fun getItemCount(): Int {
        return filteredItems.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when(holder) {
            is MainViewHolder -> holder.bind(filteredItems[position], position)
        }
    }

    inner class MainViewHolder(itemView: View): BaseViewHolder<TopAnimeEntity>(itemView) {
        override fun bind(item: TopAnimeEntity, position: Int) {
            val image = itemView.findViewById<ImageView>(R.id.imageAnime)
            val tvTitle = itemView.findViewById<TextView>(R.id.titleAnime)
            val tvRating = itemView.findViewById<TextView>(R.id.tvRating)
            val tvSource = itemView.findViewById<TextView>(R.id.tvSource)
            image.setImagePlaceholderImg(item.image, context)
            image.clipToOutline = true
            tvTitle.text = item.name
            tvRating.text = item.rating
            tvSource.text = item.source.trim()
            itemView.setOnClickListener {
                listener.goToDetail(item)
            }
        }
    }

    fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString().lowercase(Locale.US)
                filteredItems = if (charString.isEmpty()) {
                    listAnime
                } else {
                    val filteredList = mutableListOf<TopAnimeEntity>()
                    for (anime in listAnime) {
                        if (anime.name.lowercase(Locale.US).contains(charString) || anime.rating.lowercase(Locale.US).contains(charString)
                            || anime.source.lowercase(Locale.US).contains(charString) || anime.type.lowercase(Locale.US).contains(charString)) {
                            filteredList.add(anime)
                        }
                    }
                    filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = filteredItems
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                filteredItems = filterResults.values as MutableList<TopAnimeEntity>
                if (filteredItems.isEmpty()) {
                    listener.noResults(true)
                } else {
                    listener.noResults(false)
                }
                notifyDataSetChanged()
            }
        }
    }

}