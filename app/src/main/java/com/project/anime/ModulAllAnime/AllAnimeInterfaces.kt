package com.project.anime.ModulAllAnime

import com.project.anime.Models.Room.TopAnimeEntity
import com.project.anime.Utils.vo.Resource

class AllAnimeInterfaces {

    interface AllAnimeDataSourceI {
        suspend fun getAllAnime(): Resource.Success<MutableList<TopAnimeEntity>>
    }

    interface AllAnimeRepoI {
        suspend fun getAllAnime(): Resource.Success<MutableList<TopAnimeEntity>>
    }

    interface ShowAllAnimeI {
        fun goToDetail(topAnimeEntity: TopAnimeEntity)
        fun noResults(visibility: Boolean)
    }

}