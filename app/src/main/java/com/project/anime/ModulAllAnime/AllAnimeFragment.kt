package com.project.anime.ModulAllAnime

import android.app.Activity
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.constraintlayout.helper.widget.Carousel
import androidx.core.os.bundleOf
import androidx.core.view.forEach
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.chip.Chip
import com.project.anime.Models.Room.TopAnimeEntity
import com.project.anime.ModulAllAnime.Adapters.ShowAllAnimeAdapter
import com.project.anime.R
import com.project.anime.Utils.AlerterUtil
import com.project.anime.Utils.DB.AppDatabase
import com.project.anime.Utils.Extensions.ViewUtils.hide
import com.project.anime.Utils.Extensions.ViewUtils.show
import com.project.anime.Utils.vo.Resource
import com.project.anime.databinding.FragmentAllAnimesBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.imaginativeworld.whynotimagecarousel.listener.CarouselListener
import org.imaginativeworld.whynotimagecarousel.model.CarouselItem

class AllAnimeFragment : Fragment(R.layout.fragment_all_animes), AllAnimeInterfaces.ShowAllAnimeI {

    private lateinit var binding: FragmentAllAnimesBinding
    private lateinit var adapter: ShowAllAnimeAdapter
    private var listTopAnimeEntity: MutableList<TopAnimeEntity>? = null
    private var listTopAnimeFilter: MutableList<TopAnimeEntity>? = null

    private val viewModelAllAnime by viewModels<AllAnimeViewModel> { AllAnimeViewModelFactory(
        AllAnimeRepo(AllAnimeDataSource(requireContext(),  AppDatabase.getDatabase(requireActivity().applicationContext)))
    ) }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView(view)
        setupRecyclerView()
        hideToolbar()
        getAllAnime()
        setupSearch()
        listenerChips()
    }

    private fun listenerChips() {
        binding.chipGroup.forEach { child ->
            (child as? Chip)?.setOnCheckedChangeListener { _, _ ->
                hideKeyboard()
                registerFilterChanged()
            }
        }
    }

    private fun registerFilterChanged() {
        val ids = binding.chipGroup.checkedChipIds
        var title = ""
        ids.forEach { id ->
            title = binding.chipGroup.findViewById<Chip>(id).text.toString()
        }
        when(title) {
            getString(R.string.in_the_air) -> {
                listTopAnimeFilter = listTopAnimeEntity?.filter { it.isAired }?.toMutableList()
                if (!listTopAnimeFilter.isNullOrEmpty()) {
                    adapter.listAnime = listTopAnimeFilter ?: mutableListOf()
                    adapter.filteredItems = listTopAnimeFilter ?: mutableListOf()
                    adapter.notifyDataSetChanged()
                } else {
                    AlerterUtil.createAlertError(getString(R.string.no_found_results), requireActivity())
                }
            }
            getString(R.string.top_10) -> {
                listTopAnimeFilter = listTopAnimeEntity?.filter { it.rank <= 10 }?.toMutableList()
                if (!listTopAnimeFilter.isNullOrEmpty()) {
                    adapter.listAnime = listTopAnimeFilter ?: mutableListOf()
                    adapter.filteredItems = listTopAnimeFilter ?: mutableListOf()
                    adapter.notifyDataSetChanged()
                } else {
                    AlerterUtil.createAlertError(getString(R.string.no_found_results), requireActivity())
                }
            }
            getString(R.string.manga) -> {
                listTopAnimeFilter = listTopAnimeEntity?.filter { it.source == title }?.toMutableList()
                if (!listTopAnimeFilter.isNullOrEmpty()) {
                    adapter.listAnime = listTopAnimeFilter ?: mutableListOf()
                    adapter.filteredItems = listTopAnimeFilter ?: mutableListOf()
                    adapter.notifyDataSetChanged()
                } else {
                    AlerterUtil.createAlertError(getString(R.string.no_found_results), requireActivity())
                }
            }
            getString(R.string.tv) -> {
                listTopAnimeFilter = listTopAnimeEntity?.filter { it.type == title }?.toMutableList()
                if (!listTopAnimeFilter.isNullOrEmpty()) {
                    adapter.listAnime = listTopAnimeFilter ?: mutableListOf()
                    adapter.filteredItems = listTopAnimeFilter ?: mutableListOf()
                    adapter.notifyDataSetChanged()
                } else {
                    AlerterUtil.createAlertError(getString(R.string.no_found_results), requireActivity())
                }
            }
            else -> {
                if (!listTopAnimeEntity.isNullOrEmpty()) {
                    binding.noResults.hide()
                    adapter.listAnime = listTopAnimeEntity ?: mutableListOf()
                    adapter.filteredItems = listTopAnimeEntity?: mutableListOf()
                    adapter.notifyDataSetChanged()
                } else {
                    binding.noResults.show()
                }
            }
        }
    }

    private fun setupView(view: View) {
        binding = FragmentAllAnimesBinding.bind(view)
    }

    private fun getAllAnime() {
        viewModelAllAnime.getAllAnime().observe(viewLifecycleOwner, { result ->
            when(result) {
                is Resource.Loading -> {
                    lifecycleScope.launch(Dispatchers.Main) {
                        binding.progressBarAllAnime.show()
                    }
                }
                is Resource.Success -> {
                    hideProgressBar()
                    listTopAnimeEntity = result.data
                    adapter.listAnime = result.data
                    adapter.filteredItems = result.data
                    binding.rvAllAnimes.adapter = adapter
                    setupSliderImages()
                }
                is Resource.Failure -> {
                    hideProgressBar()
                }
            }
        })
    }

    private fun setupSliderImages() {
        val list = mutableListOf<CarouselItem>()
        listTopAnimeEntity?.forEach { anime ->
            val carouselItem = CarouselItem(anime.image, anime.name)
            list.add(carouselItem)
        }
        binding.carousel.carouselListener = object : CarouselListener {
            override fun onClick(position: Int, carouselItem: CarouselItem) {
                super.onClick(position, carouselItem)
                var animeId = listTopAnimeEntity?.find { it.name == carouselItem.caption }?.id ?: 0
                val bundle = bundleOf("animeId" to animeId)
                findNavController().navigate(R.id.action_allAnimeFragment_to_detailAnimeFragment, bundle)
            }
        }
        binding.carousel.addData(list)
    }

    private fun setupRecyclerView() {
        binding.rvAllAnimes.layoutManager = LinearLayoutManager(requireContext())
        binding.rvAllAnimes.addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))
        adapter = ShowAllAnimeAdapter(requireContext(), this)
    }

    private fun setupSearch() {
        binding.searchViewAnime.setOnClickListener {
            binding.viewChips.show()
            binding.searchViewAnime.isIconified = false
        }
        binding.searchViewAnime.setOnCloseListener {
            binding.viewChips.hide()
            false
        }
        binding.searchViewAnime.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }
            override fun onQueryTextChange(newText: String): Boolean {
                binding.rvAllAnimes.adapter?.run {
                    (binding.rvAllAnimes.adapter as ShowAllAnimeAdapter).getFilter().filter(newText)
                }
                return true
            }
        })
    }

    private fun hideProgressBar() {
        lifecycleScope.launch(Dispatchers.Main) {
            binding.progressBarAllAnime.hide()
        }
    }

    private fun hideToolbar() {
        (activity as AppCompatActivity?)?.supportActionBar?.hide()
    }

    override fun goToDetail(topAnimeEntity: TopAnimeEntity) {
        val bundle = bundleOf("animeId" to topAnimeEntity.id)
        findNavController().navigate(R.id.action_allAnimeFragment_to_detailAnimeFragment, bundle)
    }

    override fun noResults(visibility: Boolean) {
        if (visibility) binding.noResults.show()
        else binding.noResults.hide()
    }

    fun hideKeyboard() {
        // Check if no view has focus:
        val view = requireActivity().currentFocus
        if (view != null) {
            val inputManager = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
            inputManager.hideSoftInputFromWindow(requireActivity().currentFocus!!.windowToken, 0)
        }
    }
}