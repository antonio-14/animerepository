package com.project.anime.ModulAllAnime

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.project.anime.Utils.vo.Resource
import kotlinx.coroutines.Dispatchers

class AllAnimeViewModel(private val repo: AllAnimeRepo): ViewModel() {

    fun getAllAnime() = liveData(viewModelScope.coroutineContext + Dispatchers.Main) {
        emit(Resource.Loading())
        try {
            emit(repo.getAllAnime())
        } catch (e: Exception) {
            emit(Resource.Failure(e))
        }
    }

}

class AllAnimeViewModelFactory(private val repo: AllAnimeRepo) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(AllAnimeRepo::class.java).newInstance(repo)
    }
}