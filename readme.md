#Arquitectura: se aplico una arquitectura MVVM.
#Base de datos: Se implemento una base de datos ROOM la cual tiene todos los beneficios de sqlite y se pueda trabajar con
liveDatas y Flow
#RestAPI: Se hizo la comunicacion con la API solo cuando no tengamos registros en la api dicha conexion se genero con la libreria
Retrofit
#Navegacion: Para la navegacion entre vistas use NavigationComponents de jetpack una de las herramientas mas actualizadas y facil de usar
a la hora de navegar.
#Relacion vista-codigo: Se implemento ViewBinding para poder hacer la relacion entre el codigo y los componentes de la vista.
